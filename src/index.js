import * as d3 from 'd3';
import { sankey, sankeyCenter,
sankeyLeft,
sankeyRight,
sankeyJustify, sankeyLinkHorizontal } from 'd3-sankey';
import { uniqueId } from 'lodash';

/** transform data to give us the Sankey nodes & links
  input is:
  [stages]
    stages: {name, title, groups}
      groups: [{ name, needs }]; name is a dagJobName
        needs: [dagJobName]

  output is:
  { nodes: [node], links: [link] }
    node: { name, category }, + unused needs info
    link: { source, target, value }, with source & target node names

  CREATE NODES
  (This would be easier if the data were provided with the stage name in each group object)
  stages.groups.names -> node.name (aka dagJobName)
  stages.name -> node.category (this will change)

  CREATE LINKS
  stages.groups.name -> target
  stages.groups.needs.each -> source
  10 -> value (constant, fiddle for looks)

  DEDUPE LINKS
  Is it possible and desirable to not show that say job 4 depends on 1 and 2,
  since it implicitly depends on 2?
**/
const transformData = (data) => {
  // stages is data here

  // this unpacking / flattening could happen on the backend
  // and that would save the highest performance expense
  const groups = data.map(({ groups }, idx, stages) => {
    return groups.map((group) => {
      return { ...group, category: stages[idx].name }
    })
  }).flat();

  // In theory, groups could be processed to remove unnecessary link
  // information but I'm not sure it's a problem just now
  const nodes = groups;

  const links = groups
    .filter(({ needs }) => needs)
    .map(({ needs, name }) => {
      return needs.map((job) => {
        return { source: job, target: name, value: 10 }
      });
    })
    .flat();

  const filteredLinks = () => {
    const nodeDict = nodes.reduce((acc, node) => {
      acc[node.name] = node;
      return acc;
    }, {});

    // const getAllAncestors = (nodes, ancestors) => {
    //   const needs = nodes.map((node) => nodeDict[node].needs || '').flat().filter(Boolean);
    //
    //   if (!needs.length) {
    //     return ancestors;
    //   }
    //
    //   const cleanNeeds = needs.filter((n) => !ancestors.includes(n))
    //
    //   return getAllAncestors(cleanNeeds, [...ancestors, ...cleanNeeds]);
    // };

    const getAllAncestors = (nodes) => {
      const needs = nodes.map((node) => nodeDict[node].needs || '').flat().filter(Boolean);

      if (needs.length) {
        return [...needs, ...getAllAncestors(needs)]
      }

      return [];
    };

    return links.filter((link) => {
      const targetNode = link.target;
      const targetNodeNeeds = nodeDict[targetNode].needs;
      const targetNodeNeedsMinusSource = targetNodeNeeds.filter((need) => need !== link.source);
      const allAncestors = getAllAncestors(targetNodeNeedsMinusSource, []);

      return !(allAncestors.includes(link.source));

    });

    /*

    for every link, check out it's target
    for every target, get the target node's needs
    then drop the current link source from that list

    call a function to get all ancestors, recursively
    is the current link's source in the list of all parents
    then we drop this link

    */
  }



  return { nodes, links: filteredLinks() };
}

const color = (d) =>  {
  const col = d3.scaleOrdinal(['prepare', 'fixtures', 'test', 'review-prepare', 'qa'], d3.schemeSet3);
  return col(d.category);
}

// Sankey-fy our nodes & data
const createSankey = ({ width, height }) => {
  const sankeyGenerator = sankey()
      .nodeId((d) => d.name)
      .nodeAlign(sankeyLeft)
      .nodeWidth(15)
      .nodePadding(10)
      .extent([[1, 5], [width, height - 105]]);
  return ({ nodes, links }) => sankeyGenerator({
    nodes: nodes.map(d => Object.assign({}, d)),
    links: links.map(d => Object.assign({}, d))
   });
}

const createNodes = (svg, nodes, settings) => {
  svg.append('g')
    .attr('stroke', '#000')
  .selectAll('rect')
  .data(nodes)
  .join('rect')
    .attr('x', (d) => d.x0)
    .attr('y', (d) => d.y0)
    .attr('height', (d) => d.y1 - d.y0)
    .attr('width', (d) => d.x1 - d.x0)
    .attr('fill', color)
  .append('title')
    .text((d) => d.name);

  svg.append('g')
     .attr('font-family', 'sans-serif')
     .attr('font-size', 10)
   .selectAll('text')
   .data(nodes)
   .join('text')
     .attr('x', (d) => d.x0 < settings.width / 2 ? d.x1 + 6 : d.x0 - 6)
     .attr('y', (d) => (d.y1 + d.y0) / 2)
     .attr('dy', '0.35em')
     .attr('text-anchor', d => d.x0 < settings.width / 2 ? 'start' : 'end')
     .text((d) => d.name);
}

const createLinks = (svg, links) => {
  const link = svg.append('g')
      .attr('fill', 'none')
      .attr('stroke-opacity', 0.4)
    .selectAll('g')
    .data(links)
    .join('g')
      .style('mix-blend-mode', 'multiply')
      .on('mouseover', function (d) {
        d3.select(this).style('stroke-opacity', 1);
      })
      .on('mouseout', function (d) {
        d3.select(this).style('stroke-opacity', .4);
      });

  const gradient = link.append('linearGradient')
    .attr('id', (d) => (d.uid = uniqueId('link')))
    .attr('gradientUnits', 'userSpaceOnUse')
    .attr('x1', (d) => d.source.x1)
    .attr('x2', (d) => d.target.x0);

  gradient.append('stop')
      .attr('offset', '0%')
      .attr('stop-color', (d) => color(d.source));

  gradient.append('stop')
      .attr('offset', '100%')
      .attr('stop-color', (d) => color(d.target));

  link.append('path')
    .attr('d', sankeyLinkHorizontal())
    .attr('stroke', (d) => `url(#${d.uid})`)
    .attr('stroke-width', d => Math.max(1, d.width));
}

const drawGraph = (data) => {
  console.log('data:', data);
  console.log('transformed:', transformData(data));

  const settings = {
    width: 1000,
    height: 600,
  }

  const transformed = transformData(data);
  const { links, nodes } = createSankey(settings)(transformed);

  console.log('sankyfied', { links, nodes });

  const svg = d3.select('svg')
    .attr('viewBox', [0, 0, settings.width, settings.height])
    .attr('width', settings.width)
    .attr('height', settings.height)

  createNodes(svg, nodes, settings);
  createLinks(svg, links);
}

const onload = async () => {
  const { details: { stages }} = await d3.json('gitlabDAGData.json');
  drawGraph(stages);
}

window.addEventListener('load', onload);
